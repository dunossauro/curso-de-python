# Sobre os exercícios

## Formato de entrega de exercícios

Você deve criar um repositório no [gitlab](https://gitlab.com/) no seguinte formato:

```
.
|____semana_1
| |____exercicios_aula_1
| | |____exercicio_1.py
| | |____exercicio_n.py
| |____exercicios_aula_2
| | |____exercicio_1.py
| | |____exercicio_n.py
|____semana_2
| |____exercicios_aula_2
| | |____exercicio_1.py
| | |____exercicio_n.py
...
```
