## Problemas da semana 1

1. Faça um programa que escreva 'Olá mundo' na tela

2. Faça um programa que pergunte o nome e a idade do usuário e exiba na tela.
```
Bem vindo <nome>, fico feliz em saber que tem <idade> anos
```

3. Faça um programa para uma loja de tintas. O programa deverá pedir o tamanho em metros quadrados da área a ser pintada. Considere que a cobertura da tinta é de 1 litro para cada 3 metros quadrados e que a tinta é vendida em latas de 18 litros, que custam R$ 80,00. Informe ao usuário a quantidades de latas de tinta a serem compradas e o preço total.

4. Faça um Programa que peça 2 números inteiros e um número real. Calcule e mostre:
  - O produto do dobro do primeiro com metade do segundo .
  - A soma do triplo do primeiro com o terceiro.
  - O terceiro elevado ao cubo.


5. Faça um programa para a leitura de duas notas parciais de um aluno. O programa deve calcular a média alcançada por aluno e apresentar:
  - A mensagem "Aprovado", se a média alcançada for maior ou igual a sete.
  - A mensagem "Reprovado", se a média for menor do que sete.
  - A mensagem "Aprovado com Distinção", se a média for igual a dez.


6. Faça um programa que pergunte o preço de três produtos e informe qual produto você deve comprar, sabendo que a decisão é sempre pelo mais barato.


7. Faça um programa que receba uma string e responda se ela tem alguma vogal, se sim, quais são? E também diga se ela é uma frase ou não.


8. Faça um programa que receba uma data de nascimento (15/07/87) e imprima
```
Você nasceu em <dia> de <mes> de <ano>
```


9. Faça um programa que peça uma nota, entre zero e dez. Mostre uma mensagem caso o valor seja inválido e continue pedindo até que o usuário informe um valor válido.


10. Faça um programa que leia 5 números e informe o maior número.



11. Faça um programa que itere em uma string e toda vez que uma vogal aparecer na sua string print o seu nome entre as letras

Exemplo:
```
string = bananas
b
eduardo
n
eduardo
n
...
```


12. Faça um programa que receba uma string, com um número de ponto flutuante, e imprima qual a parte dele que não é inteira

Exemplo:
```
n = '3.14'
responsta: 14
```


13. Faça um programa que: Dada uma lista [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] e um número inteiro, imprima a tabuada desse número.


14. Faça uma programa que dada a entrada de uma lista ele faça o calculo acumulativo da mesma:

```
Exemplo de entrada: [1, 2, 3, 4]
Exemplo de saída: [1, 3, 6, 10]
```

15. Faça um programa que dada a entrada de uma lista ele calcule as combinações e nos retorne as combinações em uma nova lista.

```
Exemplo de entrada: [1, 2, 3, 4]
Exemplo de saida: [[1, 2], [1, 3], [1, 4], [2, 3], [2, 4], [3, 4]]
```

16. Faça um programa, com uma função, que calcula a média de uma lista.

```
Funções embutidas que podem te ajudar:
  - len(lista) -> calcula o tamanho da lista
  - sum(lista) -> faz o somatório dos valores
```

17. Faça um programa, com uma função, que calcula a mediana de uma lista.

```
Funções embutidas que podem te ajudar:
  - sorted(lista) -> ordena a lista
```

18. Faça um programa, com uma função que dado uma lista e uma posição da mesma faça o quartil dessa posição.

`p_index = int(p * len(lista))`

19. Faça um programa, com uma função, que calcule a dispersão de uma lista

```
Funções embutidas que podem te ajudar:
  - min(lista) -> retorna o menor valor
  - max(lista) -> retorna o maior valor
```

20. Baseando-se nos exercícios passados, monte um dicionário com os seguintes seguintes chaves:

`lista, somatório, tamanho, maior valor e menor valor
`

21. Dada uma lista de entradas de usuário de números inteiros, construa um dicionário com a lista padrão, a lista dos valores elevados ao quadrado e a lista dos valores elevados ao cubo


### Exercícios complementares

Tópicos 1-4 [Lista de exercícios Python Brasil](https://wiki.python.org.br/ListaDeExercicios)
