"""
Faça um programa que itere em uma lista de maneira imperativa
    e que armazene em uma nova lista seu valor processado
    por uma função:
"""


def double(l):
    new_l = []
    for el in l:
        new_l.append(el * 2)
    return new_l


entrada = ['foo', 'bar', 'spam', 'eggs']
assert double(entrada) == ['foofoo', 'barbar', 'spamspam', 'eggseggs']
