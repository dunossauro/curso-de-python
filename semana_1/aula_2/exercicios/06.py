"""
6. Usando os mesmos inputs do código anterior, reconstrua o problema de maneira declarativa.
Funções que pode te ajudar:
    - operator.mul
    - map()
"""
assert (
    [el * 2 for el in ['foo', 'bar', 'spam', 'eggs']] ==
    ['foofoo', 'barbar', 'spamspam', 'eggseggs']
)
