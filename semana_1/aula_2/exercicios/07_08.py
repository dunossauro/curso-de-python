"""
Faça um programa que faça a inversão de uma lista usando
    as propriedade mutáveis dessa lista (remova da lista e insira de novo)
Entrada = [1, 2, 3]
Saída = [3, 2, 1]

Coisas que podem te ajudar:
list.insert, list.append, list.remove
"""

# OO pythonic way
entrada = [1, 2, 3]
entrada.reverse()
assert entrada == [3, 2, 1]

# Functional pythonic way
entrada = [1, 2, 3]
assert list(reversed(entrada)) == [3, 2, 1]

# Imperative way
entrada = [1, 2, 3]
new_list = []
for el in entrada:
    new_list.insert(0, el)
assert new_list == [3, 2, 1]

# Pythonic way
entrada = [1, 2, 3]
assert entrada[::-1] == [3, 2, 1]
