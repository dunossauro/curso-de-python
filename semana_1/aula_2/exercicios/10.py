"""
Especifique uma função que receba um número uma string
    e retorne se na string há o mesmo número de elementos
    que foram passados no parâmetro.

f(3, ‘aaa’) -> True
f(10, ‘Batata’) -> False
"""


def f(n, string):
    return len(string) == n


assert f(3, 'aaa') == True
assert f(4, 'aaa') == False
