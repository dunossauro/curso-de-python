"""
Faça uma função que receba um número, caso esse número seja múltiplo de 3,
    retorne “queijo”, caso contrário, retorne o valor de entrada.

f(5) -> 5
f(3) -> ‘queijo’
f(6) -> ‘queijo’
"""

def f(n):
    return 'queijo' if not n % 3 else n


assert f(3) == 'queijo'
assert f(5) == 5
assert f(6) == 'queijo'
