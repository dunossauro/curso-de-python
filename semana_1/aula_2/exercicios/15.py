"""
Faça uma função que se aplique uma função duas vezes em um valor passado
reaplica(soma_2, 2) -> 6
reaplica(sub_2, 2) -> -2
"""


def reaplica(func, val):
    return func(func(val))


assert reaplica(lambda x: x + 2, 2) == 6
assert reaplica(lambda x: x - 2, 2) == -2
