def reverse_worlds(frase):
    lista = frase.split(' ')
    return " ".join(list(reversed(lista)))
