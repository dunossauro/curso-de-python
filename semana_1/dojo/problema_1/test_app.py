from unittest import TestCase
from .app import reverse_worlds


class TestReverseWorlds(TestCase):
    def test_1(self):
        self.assertEqual(reverse_worlds('oi'), 'oi')

    def test_2(self):
        self.assertEqual(reverse_worlds('Olá bb'), 'bb Olá')

    def test_3(self):
        self.assertEqual(
            reverse_worlds('voadoras Batatinhas'), 'Batatinhas voadoras'
        )
