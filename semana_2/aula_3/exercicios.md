## Problemas Aula 3

1. Dada a função de soma

```Python
def soma(x, y):
    return x + y
```

Como ela poderia ser testada?

2. Dada a função de soma

```Python
def soma(x, y):
    return x + y
```

Faça assertivas de valores que você acredita serem coerentes com a função

3. Dado o código:

```Python
class Calc:
  def add(self, x, y):
    return x + y

  def sub(self, x, y):
    return x + y

  def mult(self, x, y):
    return x + y

  def div(self, x, y):
    return x + y
```

Faça assertivas em relação ao que funciona e não funciona nos seus casos de teste

4. Crie sua primeira suite de testes para a classe da calculadora.

5. Data a classe da calculadora. Defina difentes SUTs e os teste individualmente.

6. Agora que você tem os testes para garantir o comportamaento dos seus métodos. Você poderia efeuar a correção da classe sem grandes problemas?

7. Dada que exista uma função de soma e uma de subtração.

  Crie uma função chamada ‘exp’ que receba x, y e z. E calcule:

  `f(x, y, z) -> x + y - z`

  Faça o teste dessa função, testando seus inputs e outputs indiretos

8. Agora que você já contruiu a função ‘exp’ que tal fazer uma chamada de uma expressão mais complexa?

  `f(x, y, z) -> exp(exp(x, y, z), y, z)``

  Agora tente efetuar os testes dessa função

9. Usando a função ‘exp’ criada no problema 7. Teste a validação indireta da função de soma usando um dummy

10. Usando a função ‘exp’ criada no problema 8. Teste a validação indireta da função de subtração usando um dummy

11. Usando a função ‘exp’. Faça a checagem dos inputs indiretos da função de soma em um teste. Em outro teste faça a função dos inputs indiretos da função de sub.

  ```Python
  def exp(x, y, z):
    return sub(soma(x, y), z)
  ```
