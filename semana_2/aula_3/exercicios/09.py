from unittest import TestCase


def soma(x, y):
    return x + y


def sub(x, y):
    return x - y


def exp(x, y, z):
    return sub(soma(x, y), z)


class TestExp(TestCase):
    dummy = 0

    def test_exp_input_indireto_soma(self):
        self.assertEqual(exp(self.dummy, self.dummy, 5), -5)

    def test_exp_input_indireto_sub(self):
        self.assertEqual(exp(1, 1, self.dummy), 2)
