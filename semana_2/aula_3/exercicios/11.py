from unittest import TestCase, mock


def soma(x, y):
    return x + y


def sub(x, y):
    ...


def exp(x, y, z):
    return sub(soma(x, y), z)


class TestExp(TestCase):
    def test_exp_input_indireto_de_soma_deve_receber_x_y(self):
        x = 1
        y = 2
        z = 3
        with mock.patch('semana_2.aula_3.exercicios.11.soma') as mocked_soma:
            exp(x, y, z)

        mocked_soma.assert_called_with(x, y)

    def test_exp_input_indireto_de_sub_deve_receber_3_z(self):
        x = 1
        y = 2
        z = 3

        with mock.patch('semana_2.aula_3.exercicios.11.sub') as mocked_sub:
            exp(x, y, z)

        mocked_sub.assert_called_with(3, z)
