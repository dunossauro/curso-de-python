var = 1

func = lambda x: x + var

assert func(1) == 2
var = 2
assert func(1) == 2
