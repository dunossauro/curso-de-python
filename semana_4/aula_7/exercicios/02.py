val = 1

def func(x):
    global val
    val += 1
    return x + val


assert func(2) == 4
assert func(2) == 4
