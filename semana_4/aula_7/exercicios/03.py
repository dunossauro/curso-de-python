from unittest import TestCase, mock
import builtins


def oi():
    nome = input('Diga seu nome')
    print(f'Olá {nome}')



class TestIO(TestCase):
    @mock.patch('builtins.input', return_value='Eduardo')
    @mock.patch('builtins.print')
    def test_oi_deve_digitar_algo(self, m_out, m_in):
        oi()
        m_in.assert_called_with('Diga seu nome')
        m_out.assert_called_with('Olá Eduardo')
